"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Tasks.find({}, {
            sort: {
                createdAt: -1
            }
        });
    },

    image: function () {
        var image = Collections.Images.find({
            _id: this.ImageID
        }).fetch();

        return image[0].url();
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    }
});